const int VR = 3;
void HBridgeDrive (unsigned long);

void setup(){
  pinMode(33,OUTPUT);
  digitalWrite(33,HIGH);
  pinMode(VR,INPUT_ANALOG);
}
void loop(){
  int val = 0;
  val = analogRead(VR);
  val = map(val,0,4095,0,131071);
  
  HBridgeDrive(val);
}





void HBridgeDrive(int DriveVal){
  //HBridge駆動関数
  //DriveVal 0～131071をうけとって、HBridgeをドライブする。
  //H1,H2はTLP591,L1,L2はTLP250 PWMはL1,L2
  //センターはフリー
  
  //pin設定
  const int H1 = 8;
  const int H2 = 9;
  const int L1 = 10;
  const int L2 = 11;
  
  pinMode(H1,OUTPUT);
  pinMode(H2,OUTPUT);
  pinMode(L1,PWM);
  pinMode(L2,PWM);
  
  int DriveCentor = 65535;
  int PWMVal = 0;
  //delayTime
  int t = 30;
  static int  DFlag;

  //センター
  if(DriveVal==DriveCentor){
    if(DFlag != 0)
    {
      //dedTime
      //全閉塞
      digitalWrite(H1,LOW);
      digitalWrite(H2,LOW);
      pwmWrite(L1,0);
      pwmWrite(L2,0);
      delay(t);
    }
    //センター駆動
    digitalWrite(H1,LOW);
    digitalWrite(H2,LOW);
    pwmWrite(L1,0);
    pwmWrite(L2,0);
    DFlag = 0;

  }
  else{
    //小側
    if(DriveVal<DriveCentor){
      PWMVal = (map(DriveVal,(DriveCentor - 1),0,0,65535));
      //PWMVal = constrain(PWMVal,0,65535);
      //constrain関数バグ在リ
      
      if(DFlag != 1)
      {
        //dedTime
        //全閉塞
        digitalWrite(H1,LOW);
        digitalWrite(H2,LOW);
        pwmWrite(L1,0);
        pwmWrite(L2,0);
        delay(t);
      }
      //小側駆動
      digitalWrite(H1,HIGH);
      digitalWrite(H2,LOW);
      pwmWrite(L1,0);
      pwmWrite(L2,PWMVal);
      DFlag = 1;
    }
    if(DriveCentor<DriveVal){
      PWMVal = (map(DriveVal,(DriveCentor + 1),131071,0,65535));
      //PWMVal = constrain(PWMVal,0,65535);
      //constrain関数バグ在リ
      if(DFlag != 2)
      {
        //dedTime
        //全閉塞
        digitalWrite(H1,LOW);
        digitalWrite(H2,LOW);
        pwmWrite(L1,0);
        pwmWrite(L2,0);
        delay(t);
      }
      //大側駆動
      digitalWrite(H1,LOW);
      digitalWrite(H2,HIGH);
      pwmWrite(L1,PWMVal);
      pwmWrite(L2,0);
      DFlag = 2;
    }
    
  }


}





